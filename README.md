# Terraform Setup

## Prerequisites

- Cloud service for servers (This example uses Digital Ocean)
- Account with GitLab
- Knowledge of Terraform
- Understanding of CI/CD and usage within GitLab

## Getting started

### Resources for this project running

- [Gitlab Resources](https://docs.gitlab.com/ee/user/infrastructure/iac/)
- [Terraform Resources](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs)


