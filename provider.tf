terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
  backend "http" {}
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_droplet" "web" {
  count  = 0
  image  = "ubuntu-18-04-x64"
  name   = "web-${count.index}"
  region = "nyc1"
  size   = "s-1vcpu-1gb"

  ssh_keys = [
    data.digitalocean_ssh_key.do_key_01.id
  ]

  provisioner "remote-exec" {
    connection {
      host        = self.ipv4_address
      type        = "ssh"
      user        = "root"
      private_key = var.ssh_key
      timeout     = "1m"
    }

    inline = ["sudo apt update", "echo Done!"]
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key ${var.pvt_key} -e 'pub_key=${var.pub_key}' ./ansible/apache-install.yml"
  }

}
