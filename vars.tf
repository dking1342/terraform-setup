
variable "do_token" {}
variable "pvt_key" {}
variable "pub_key" {}
variable "ssh_key" {
  default = "ssh_key"
}
